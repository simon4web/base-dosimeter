
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <SPI.h>

//#define DEBUG

// I/O PINS
#define DRDYOUT 2 //D8 to D13
#define CE 10 //Chip Enable

// COMMANDS
#define COM_SPEED 'S' //followed by two bytes for speed (in MAX11040k form)
#define COM_CONF 'C'  //followed by two bytes for config (first conf byte, in MAX11040k form, second 0)
#define COM_BEGIN 'B' //followed by two bytes 0x00,0x00
#define COM_PAUSE 'P' //followed by two bytes 0x00,0x00

#define SAM_INST_REG 0b01000000
#define DATA_RATE_REG 0b01010000
#define CONF_REG 0b01100000
#define DATA_REG 0b01110000
#define READ_OP 0x80
#define WRITE_OP 0x00

#define BUFFER_SIZE 3*500L

#define SPISET SPISettings(4000000, MSBFIRST, SPI_MODE2)

volatile bool fallingDRDYOUT = false;
volatile uint8_t command[3];
volatile uint8_t receivedSize = 0;
volatile bool receivedCom = false;
volatile bool transActive = false;

//uint8_t buf[BUFFER_SIZE] = {0};
//uint16_t bufPointer = 0;
uint8_t b[3];

void setup()
{
  cli();
  pinMode(DRDYOUT, INPUT);
  pinMode(CE, OUTPUT);
  pciSetup(DRDYOUT);
  SPI.begin();
  Serial.begin(2000000);
  #ifdef DEBUG
    Serial.print("CONTEST");
    SPI.beginTransaction(SPISET);
    digitalWrite(CE, LOW);
    delayMicroseconds(1);
    SPI.transfer(CONF_REG|WRITE_OP);
    SPI.transfer(0b00110000);
    delayMicroseconds(1);
    digitalWrite(CE, HIGH);
    SPI.endTransaction();
    SPI.beginTransaction(SPISET);
    digitalWrite(CE, LOW);
    delayMicroseconds(1);
    SPI.transfer(DATA_RATE_REG|WRITE_OP);
    b[0]=SPI.transfer(0b10100000);
    b[1]=SPI.transfer(0b00000000);
    delayMicroseconds(1);
    digitalWrite(CE, HIGH);
    SPI.endTransaction();
    SPI.beginTransaction(SPISET);
    digitalWrite(CE, LOW);
    delayMicroseconds(1);
    SPI.transfer(DATA_RATE_REG|READ_OP);
    b[0]=SPI.transfer(0b00000000);
    b[1]=SPI.transfer(0b00000000);
    delayMicroseconds(1);
    digitalWrite(CE, HIGH);
    SPI.endTransaction();
  #endif
  sei();
}

void loop()
{

  if (fallingDRDYOUT && transActive)
  {
    fallingDRDYOUT = false;
    SPI.beginTransaction(SPISET);
    digitalWrite(CE, LOW);
    delayMicroseconds(1);
    SPI.transfer(DATA_REG|READ_OP);
    b[0] = SPI.transfer(0x00);
    b[1] = SPI.transfer(0x00);
    b[2] = SPI.transfer(0x00);
    digitalWrite(CE, HIGH);
    SPI.endTransaction();
    Serial.write( b[0]);
    Serial.write( b[1]);
    Serial.write( b[2]);
  }
  if (receivedCom)
  {
    receivedCom = false;
    SPI.beginTransaction(SPISET);
    digitalWrite(CE, LOW);
    delayMicroseconds(1);
    if(command[0] == COM_SPEED)
    {
      SPI.transfer(DATA_RATE_REG|WRITE_OP);
      SPI.transfer(command[1]);
      SPI.transfer(command[2]);
    }
    if(command[0] == COM_CONF)
    {
      SPI.transfer(CONF_REG|WRITE_OP);
      SPI.transfer(command[1]);
    }
    if(command[0] == COM_BEGIN)
    {
      transActive = true;
      SPI.transfer(DATA_REG|READ_OP);
      b[0] = SPI.transfer(0x00);
      b[1] = SPI.transfer(0x00);
      b[2] = SPI.transfer(0x00);
    }
    if(command[0] == COM_PAUSE)
    {
      SPI.transfer(CONF_REG|WRITE_OP);
      SPI.transfer(0x00);
    }
    delayMicroseconds(1);
    digitalWrite(CE, HIGH);
    SPI.endTransaction();
  }
  /*sleep_IDLE();*/
}



ISR (PCINT2_vect) // handle pin change interrupt for D8 to D13, here used for DRDYOUT
{
  if(digitalRead(DRDYOUT) == LOW)
  {
    fallingDRDYOUT = true;
  }
}

void serialEvent()
{
  while (Serial.available())
  {
   command[receivedSize] = Serial.read(); // Fetch the received byte
   if(receivedSize == 2)
   {
       receivedCom = true;
       receivedSize = 0;
   }
   else
   {
      receivedSize++;
   }
  }
}

void pciSetup(byte pin)
{
  *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // enable pin
  PCIFR  |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
  PCICR  |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}

void sleep_IDLE()
{
    set_sleep_mode(SLEEP_MODE_IDLE);//define power down sleep mode
    sleep_enable();
    sleep_cpu();//Set sleep enable (SE) bit, this puts the ATmega to sleep
    sleep_disable();
}
