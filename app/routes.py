from flask import render_template, flash, redirect, url_for, send_file
from datetime import datetime,timedelta
import pytz
import os
import time
from app import app
from app.forms import TimeChangeForm, CalibrateForm, MsmtForm, AnaForm, DelForm, DownloadForm
from bd_serial.controller import Controller
from bd_serial.analyzer import Analyzer

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/timeset', methods=['GET', 'POST'])
def timeset():
    form = TimeChangeForm()
    utctime = datetime.utcnow()
    localtime = pytz.utc.localize(utctime, is_dst=None).astimezone(pytz.timezone("Europe/Vienna"))
    localtime_string = localtime.strftime("%Y-%m-%d %H:%M:%S")
    if (form.validate_on_submit()):
        naive_time = datetime.fromisoformat(form.iso_string.data)
        localTimezone = pytz.timezone("Europe/Vienna")
        aware_time = localTimezone.localize(naive_time, is_dst=None)
        utcDateTime = aware_time.astimezone(pytz.utc)
        os.popen('sudo date --set="%s"' % (utcDateTime), 'w').write('dosi123') #change system time (root required)
        flash('Time Channged to {}'.format(form.iso_string.data))
        time.sleep(0.1) # wait for time to be set
        return redirect(url_for('timeset'))
    return render_template('timeset.html', title = "Time", loctime = localtime_string, form=form)

@app.route('/calib', methods=['GET', 'POST'])
def calib():
    form = CalibrateForm()
    if (form.validate_on_submit()):
        con = Controller()
        con.setup_meassurement("Calib","now",60,12000)
        flash('Calibartion started.')
        return redirect(url_for('calib'))
    return render_template('calib.html', title = "Calibration", form=form)

@app.route('/msmt', methods=['GET', 'POST'])
def msmt():
    form = MsmtForm()
    con = Controller()
    msmt_conf = con.get_meassurement()
    utctime = datetime.utcnow()
    localtime = pytz.utc.localize(utctime, is_dst=None).astimezone(pytz.timezone("Europe/Vienna"))
    localtime_string = localtime.strftime("%Y-%m-%d %H:%M:%S")
    percentage = None
    if (msmt_conf != None):
        localTimezone = pytz.timezone("Europe/Vienna")
        start_time = localTimezone.localize(datetime.fromisoformat(msmt_conf["start_time"]), is_dst=None)
        msmt_conf["end_time"] = start_time + timedelta(seconds=msmt_conf["duration"])
        if (localtime > start_time and localtime < start_time + timedelta(seconds=msmt_conf["duration"])):
            percentage = int((localtime - start_time).total_seconds()*100/msmt_conf["duration"])
    if (form.validate_on_submit()):
        con.setup_meassurement(form.name.data,form.iso_string.data,timedelta(minutes = form.minutes.data, hours = form.hours.data,days = form.days.data).total_seconds(),form.fs.data)
        flash('Meassurement "' + form.name.data + '" is planed for ' + form.iso_string.data)
        return redirect(url_for('msmt'))
    return render_template('msmt.html', title = "Meassurement Planing", msmt_conf = msmt_conf, percentage = percentage, loctime = localtime_string, form=form)

@app.route('/ana', methods=['GET', 'POST'])
def ana():
    ana_form = AnaForm()
    del_form = DelForm()
    dl_form = DownloadForm()
    msmtfiles = os.listdir("/home/pi/msmts")
    ana_disps = []
    for file in msmtfiles:
        if (".json" in file):
            name = file.split(".")[0]
            #if (name != "Calib"):
            ana = Analyzer(name,"/home/pi/msmts/") #add analyzer object to list
            ana_disps.append(ana.get_info())
    ana_form.name.choices = [(ana["name"],ana["name"]) for ana in ana_disps]
    del_form.name.choices = [(ana["name"],ana["name"]) for ana in ana_disps]
    dl_form.name.choices = [(ana["name"],ana["name"]) for ana in ana_disps]

    if (ana_form.submit.data and ana_form.validate_on_submit()):
        print("got to validate ana")
        ana = Analyzer(ana_form.name.data,"/home/pi/msmts/")
        ana.analyze(win_func=ana_form.win_func.data, win_size_seconds = ana_form.win_size.data)
        flash('Meassurement "' + ana_form.name.data + '" is now analyzed!')
        return redirect(url_for('ana'))
    if (dl_form.submit.data and dl_form.validate_on_submit()):
        print("got to validate download")
        ana = Analyzer(ana_form.name.data,"/home/pi/msmts/")
        if (not ana.is_analyzed):
            flash('Meassurement "' + ana_form.name.data + '" is not analyzed!')
            return redirect(url_for('ana'))
        else:
            ana.create_pdf("/tmp/" + ana.name + ".pdf")
            return send_file("/tmp/" + ana.name +".pdf", as_attachment=True)
    if (del_form.submit.data and del_form.validate_on_submit()):
        try:
            os.remove("/home/pi/msmts/" + del_form.name.data + ".bin")
        except:
            pass
        if (not del_form.only_bin.data):
            try:
                os.remove("/home/pi/msmts/" + del_form.name.data + ".json")
            except:
                pass
            flash('Meassurement "' + del_form.name.data + '" deleted.')
        else:
            flash('Raw Data of Meassurement "' + del_form.name.data + '" deleted.')
        return redirect(url_for('ana'))
    return render_template('ana.html', title = "Analyze", ana_disps = ana_disps, ana_form=ana_form, del_form = del_form, dl_form=dl_form  )

@app.route('/error')
def error():
    return "ERROR"
