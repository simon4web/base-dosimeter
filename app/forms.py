from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, SelectField, BooleanField
from wtforms.validators import DataRequired, InputRequired

class TimeChangeForm(FlaskForm):
    iso_string = StringField('New Time', validators=[DataRequired()])
    submit = SubmitField('Change Time')

class CalibrateForm(FlaskForm):
    submit = SubmitField('Do Calibation')

class MsmtForm(FlaskForm):
    iso_string = StringField('Start Time', validators=[DataRequired()]) #, validators=[DataRequired()]
    name = StringField('Name', validators=[DataRequired()])
    fs = IntegerField('Sampling Frequency', validators=[InputRequired()])
    minutes = IntegerField('minutes', validators=[InputRequired()])
    hours = IntegerField('hours', validators=[InputRequired()])
    days = IntegerField('days', validators=[InputRequired()])
    submit = SubmitField('Plan Meassurement')

class AnaForm(FlaskForm):
    name = SelectField('Name')
    win_func = SelectField('Window Function', choices=[('hamming', 'Hamming'), ('hanning', 'Hanning'), ('blackman', 'Blackman'), ('bartlett', 'Bartlett')])
    win_size = IntegerField('Window Size')
    submit = SubmitField('Analyze')

class DelForm(FlaskForm):
    name = SelectField('Name')
    only_bin = BooleanField('Delete only Raw Data')
    submit = SubmitField('Delete')

class DownloadForm(FlaskForm):
    name = SelectField('Name')
    submit = SubmitField('Download Analysis Report')
