import RPi.GPIO as GPIO
def my_callback(channel):
    print('This is a edge event callback function!')

GPIO.setmode(GPIO.BOARD)
GPIO.setup(15, GPIO.IN)

#GPIO.add_event_detect(15, GPIO.FALLING, callback=my_callback)
while (True):
    GPIO.wait_for_edge(15, GPIO.FALLING)
    print("detected")
    pass
