import spidev
import math



class Max11040k(spidev.SpiDev):
    """docstring for max11040k."""
    ################ Constants ################
    ### Command Bytes ###
    # DATASHEET PAGE 18
    __SAM_INST_REG = 0b01000000
    __DATA_RATE_REG = 0b01010000
    __CONF_REG = 0b01100000
    __DATA_REG = 0b01110000
    __READ_OP = 0x80
    __WRITE_OP = 0x00
    ### Configuration Register ###
    # DATASHEET PAGE 19
    # need to be accessible from outside
    SHDN = (1<<7)
    RST = (1<<6)
    EN24BIT = (1<<5)
    XTALEN = (1<<4)
    FAULTDIS = (1<<3)
    PDBUF = (1<<2)
    ### Datarate Calc ###
    # DATASHEET PAGE 23 and following
    __FXIN = 24576000
    __COARSE_CYCLE_FACTOR={
        1   : 0b111,
        2   : 0b110,
        4   : 0b000,
        8   : 0b101,
        16  : 0b100,
        32  : 0b011,
        64  : 0b010,
        128 : 0b001,
    }

    def __init__(self):
        super().__init__()

    def start(self,spibusnum,spidevnum,speed):
        self.open(spibusnum,spidevnum)
        self.mode = 0b10
        self.max_speed_hz = speed

    def config(self, confbits, samplerate):
        #CONF_REG
        confbyte = 0x00
        for bit in confbits:
            confbyte = confbyte|bit
        self.writebytes([self.__CONF_REG|self.__WRITE_OP,confbyte])

        #DATA_RATE_REG
        if (samplerate != None):
            div = self.__FXIN/samplerate
            demandedCcf = div/384
            print (demandedCcf)
            #find next cff smaller than demandedCcf
            for ccf in self.__COARSE_CYCLE_FACTOR:
                if(demandedCcf < ccf):
                    demandedCcf = ccf/2
                    break
            print (demandedCcf)
            fsampc = self.__COARSE_CYCLE_FACTOR[demandedCcf]
            demandedFcf = math.ceil(demandedCcf/4)
            fsampf = math.floor((div-(demandedCcf*384))/demandedFcf)
            drbytes = fsampf | fsampc<<13
            self.writebytes([self.__DATA_RATE_REG|self.__WRITE_OP,drbytes >> 8, drbytes & 0xFF])

    def getConfig(self):
        resp = self.xfer2([self.__CONF_REG|self.__READ_OP,0x00])
        print("Set Bits:")
        if(resp[1]&self.SHDN):
            print("SHDN")
        if(resp[1]&self.RST):
            print("RST")
        if(resp[1]&self.EN24BIT):
            print("EN24BIT")
        if(resp[1]&self.XTALEN):
            print("XTALEN")
        if(resp[1]&self.FAULTDIS):
            print("FAULTDIS")
        if(resp[1]&self.PDBUF):
            print("PDBUF")

        resp = self.xfer2([self.__DATA_RATE_REG|self.__READ_OP,0x00,0x00])
        print (resp)

    def sample(self):
        resp = self.xfer2([self.__DATA_REG|self.__READ_OP,0x00,0x00,0x00])
        #print (resp)
