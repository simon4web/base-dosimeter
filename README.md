# Base Dosimeter

## Folder Structure
+ arduino_firmware  
  Code for the Arduino Nano, functioning as a serial-SPI Bridge between ADC and Raspberry Pi
+ bd_serial  
  contains Serial connection on Raspberry Pi
+ bd_spi (deprecated)  
  contains tests for direct communication between Raspberry Pi and ADC. Not used due to slow interrupt handling of the Raspberry Pi
+ Raspberry_Setup  
  contains files for first time setup of the Raspberry Pi (see "Raspberry PI Setup")
+ test_arduino_funcs (deprecated)  
  contains test functions for the use of Arduino Nano internal ADC.
+ app  
  contains the web application.
+ msmt  
  contains the measurements data of the dosimeter.
+ sample_pics  
  pictures made during development
+ 3rd-party-software  
  contains all 3rd-party libraries

## Raspberry PI Setup
1. OS:  
    Raspbian Buster Lite 2019-09-26 (https://www.raspberrypi.org/downloads/raspbian/)  
    flash with Balena Etcher 1.5.53 (https://www.balena.io/etcher/)
2. SSH access:  
    place files from folder Raspberry_Setup in "boot" partition (https://www.raspifun.de/viewtopic.php?t=4)  
    *Note: Enter right SSID and password in wpa_supplicant.conf*
3. Boot RaspberryPi and connect via SSH  
    `ssh pi@raspberrypi`
4. Change Password  
    `passwd`
5. Change Hostname:  
    + replace `raspberry` in /etc/hostname and /etc/hosts with desired hostname (here: `basedosimeter`) (https://blog.jongallant.com/2017/11/raspberrypi-change-hostname/)
    + reboot Raspberry  
    `sudo reboot`
6. Connect via SSH  
    `ssh pi@basedosimeter`
7. Enable Serial  
    + add lines `enable_uart=1` and `dtoverlay=pi3-miniuart-bt` in /boot/config.txt
    + disable login over uart via raspberry config tool
    `sudo raspberry-config`
    (http://www.netzmafia.de/skripten/hardware/RasPi/RasPi_Serial.html)
8. Enable SPI (deprecated)  
    uncomment line `dtparam=spi=on` in /boot/config.txt (https://learn.sparkfun.com/tutorials/raspberry-pi-spi-and-i2c-tutorial/all)
9. Install dependency for numpy  
    `sudo apt-get install libatlas3-base`
10. disable timesync from ntp server  
    `sudo systemctl stop systemd-timesyncd`  
    `sudo systemctl disable systemd-timesyncd`  
11. set timezone for cron  
    add line `TZ="UTC"` in /etc/default/cron  
12. Setup Wifi AP: (https://www.az-delivery.de/blogs/azdelivery-blog-fur-arduino-und-raspberry-pi/raspberry-pi-als-accesspoint)  
    + add following lines in /etc/network/interfaces  

          `auto lo`  
          `iface lo inet loopback`  
          `auto eth0`  
          `iface eth0 inet dhcp`  
          `allow-hotplug wlan0`  
          `iface wlan0 inet static`  
          `address 192.168.222.1`  
          `netmask 255.255.255.0`  

    + add lines `denyinterfaces wlan0` in /etc/dhcpcd.conf  
    + reboot  
    + install dnsmasq  
      `sudo apt-get install dnsmasq`
    + replace all lines in /etc/dhcpcd.conf with  

          `interface=wlan0`  
          `no-dhcp-interface=eth0`  
          `dhcp-range=192.168.222.10,192.168.222.200,12h`  
          `dhcp-option=option:dns-server,192.168.222.1`  

    + enable dnsmasq  
      `sudo systemctl enable dnsmasq`
    + reboot  
    + install hostapd  
      `sudo apt-get install hostapd`
    + add following lines in /etc/hostapd/hostapd.conf:   

          `interface=wlan0`  
          `ssid=BaseDosimeter`  
          `channel=1`  
          `hw_mode=g`  
          `ieee80211n=1`  
          `ieee80211d=1`  
          `country_code=DE`  
          `wmm_enabled=1`  

          `auth_algs=1`  
          `wpa=2`  
          `wpa_key_mgmt=WPA-PSK`  
          `rsn_pairwise=CCMP`  
          `wpa_passphrase=ENTER_PASSWORD_HERE`  

    + add following lines in /etc/default/hostapd:    

          `RUN_DAEMON=yes`  
          `DAEMON_CONF="/etc/hostapd/hostapd.conf"`  

    + start and enable hostapd    
      `sudo systemctl start hostapd`   
      `sudo systemctl enable hostapd`  
13. copy all files and folders from https://bitbucket.org/simon4web/base-dosimeter/src/master/ to /home/pi  
---

## PYTHON 3 VENV Setup

https://tecadmin.net/use-virtualenv-with-python3/  
1. Install pip3:  
    `sudo apt-get install python3-pip`  
2. Install venv:  
    `pip3 install virtualenv`  
3. Add python location to PATH:  
    Add line `export PATH="$PATH:/home/pi/.local/bin"` to /.profile  
4. Roboot RaspberryPi  
5. Create new venv:    
    `virtualenv -p /usr/bin/python3 baseEnv`  
6. Activate venv:    
    `source baseEnv/bin/activate`    
7. Install libraries:  
    `pip3 install --no-index --find-links=/home/pi/3rd-party-software`  

## Autostart Base Dosimeter
+ create file /etc/systemd/system/basedosimeter.service
+ add following lines in /etc/systemd/system/basedosimeter.service  

      `[Unit]`  
      `Description=Base Dosimeter`  
      `After=network.target`  

      `[Service]`  
      `User=pi`  
      `ExecStart=/home/pi/flask_start.sh`  
      `Restart=always`  

      `[Install]`  
      `WantedBy=multi-user.target`  

+ start and enable service
  `sudo systemctl daemon-reload`
  `sudo systemctl start basedosimeter`
  `sudo systemctl enable basedosimeter`
