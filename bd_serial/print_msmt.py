def sign_extend(value, bits):
    sign_bit = 1 << (bits - 1)
    return (value & (sign_bit - 1)) - (value & sign_bit)

n=1
m=10
with open('/home/pi/msmts/Test.bin','rb') as f:
    i=0
    while (i<n): # windows
        j=0
        while (j<m): # Values in a window
            line = f.read(3)
            print(sign_extend(line[0]<<16 | line[1] << 8 | line[2],24))
            j+=1
        i+=1
