import serial
import math

def sign_extend(value, bits):
    sign_bit = 1 << (bits - 1)
    return (value & (sign_bit - 1)) - (value & sign_bit)


class Max11040k(serial.Serial):
    ################ Constants ################
    ### Configuration Register ###
    # DATASHEET PAGE 19
    # need to be accessible from outside
    SHDN = (1<<7)
    RST = (1<<6)
    EN24BIT = (1<<5)
    XTALEN = (1<<4)
    FAULTDIS = (1<<3)
    PDBUF = (1<<2)
    ### Datarate Calc ###
    # DATASHEET PAGE 23 and following
    __FXIN = 24576000
    __COARSE_CYCLE_FACTOR={
        1   : 0b111,
        2   : 0b110,
        4   : 0b000,
        8   : 0b101,
        16  : 0b100,
        32  : 0b011,
        64  : 0b010,
        128 : 0b001,
    }

    def __init__(self):
        super().__init__()

    def setup(self,baud,port):
        self.baudrate = baud
        self.port = port
        self.open()


    def config(self, confbits, samplerate):
        #CONF_REG
        confbyte = 0x00
        for bit in confbits:
            confbyte = confbyte|bit
        self.write(serial.to_bytes([ord('C'),confbyte,0x00]))

        #DATA_RATE_REG
        if (samplerate != None):
            div = self.__FXIN/samplerate
            demandedCcf = div/384
            #find next cff smaller than demandedCcf
            for ccf in self.__COARSE_CYCLE_FACTOR:
                if(demandedCcf < ccf):
                    demandedCcf = ccf/2
                    break
            fsampc = self.__COARSE_CYCLE_FACTOR[demandedCcf]
            demandedFcf = math.ceil(demandedCcf/4)
            fsampf = math.floor((div-(demandedCcf*384))/demandedFcf)
            drbytes = fsampf | fsampc<<13
            self.write(serial.to_bytes([ord('S'),drbytes >> 8, drbytes & 0xFF]))

    def beginSampling(self):
        self.write(serial.to_bytes([ord('B'),0x00,0x00]))

    def pauseSampling(self):
        self.write(serial.to_bytes([ord('P'),0x00,0x00]))

    def getSampleNum(self):
        while(self.in_waiting < 2):
            pass
        line = self.read(3)
        return sign_extend(line[0]<<16 | line[1] << 8 | line[2],24)

    def getSampleBytes(self):
        while(self.in_waiting < 2):
            pass
        line = self.read(3)
        return line
