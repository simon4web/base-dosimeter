import max11040k
import time

adc = max11040k.Max11040k()
adc.setup(2000000,'/dev/ttyAMA0')
adc.pauseSampling()
adc.config([adc.EN24BIT,adc.XTALEN],12000)
adc.beginSampling()
i=0
start = int(round(time.time() * 1000))
while (int(round(time.time() * 1000))-start < 1000):
    (adc.getSampleNum())
    i += 1
print(i)
adc.pauseSampling()
