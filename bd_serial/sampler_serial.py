import max11040k
import os
import json

with open('/tmp/msmt_conf.json', 'r') as f:
    msmt_conf = json.load(f)
msmt_conf["done"] = False
# Saving config for sampler
with open('/home/pi/msmts/' + msmt_conf["name"] + '.json', 'w+') as f:
    json.dump(msmt_conf, f)

adc = max11040k.Max11040k()
adc.setup(2000000,'/dev/ttyAMA0')
adc.pauseSampling()
adc.config([adc.EN24BIT,adc.XTALEN],msmt_conf["fs"])
adc.beginSampling()
with open('/home/pi/msmts/'+msmt_conf["name"]+'.bin','wb') as f:
    for i in range(30):
        adc.getSampleBytes() # dump first 30 values
    j=0
    while (j<msmt_conf["duration"]): #total_seconds
        j += 1
        i=0
        while (i<msmt_conf["fs"]):
            f.write(adc.getSampleBytes())
            i += 1
adc.pauseSampling()

with open('/home/pi/msmts/' + msmt_conf["name"] + '.json', 'r') as f:
    msmt_conf = json.load(f)
msmt_conf["done"] = True
# Saving config for sampler
with open('/home/pi/msmts/' + msmt_conf["name"] + '.json', 'w+') as f:
    json.dump(msmt_conf, f)

os.remove("/tmp/msmt_conf.json")
