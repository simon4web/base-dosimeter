from datetime import datetime, date, time, timedelta
import pytz
import json
from crontab import CronTab
import subprocess

class Controller():

    __sampler_script = "/home/pi/sampler_serial_start.sh"
    def __init__(self):
        pass

    def __local_ISO_2_datetime(self,Iso_start_time):
        naivestart_time = datetime.fromisoformat(Iso_start_time) #https://stackoverflow.com/questions/79797/how-to-convert-local-time-string-to-utc
        localTimezone = pytz.timezone("Europe/Vienna")
        awarestart_time = localTimezone.localize(naivestart_time, is_dst=None)
        utcDateTime = awarestart_time.astimezone(pytz.utc)
        return utcDateTime

    def setup_meassurement(self,name,start_time,duration,fs):
        # Creating Dictonary for sampler
        msmt_conf = {}
        msmt_conf["name"] = name
        msmt_conf["start_time"] = start_time
        msmt_conf["duration"] = duration #timedelta(days=0,seconds=0,microseconds=0,milliseconds=0,minutes=duration,hours=0,weeks=0).total_seconds() #in seconds
        msmt_conf["fs"] = fs #Sps

        # Saving config for sampler
        with open('/tmp/msmt_conf.json', 'w+') as f:
            json.dump(msmt_conf, f)


        if (start_time == "now"):
            subprocess.Popen([self.__sampler_script]) # start sampler in background
        else:
            # Setting cron for Sampler
            cron = CronTab(user='pi')
            cron.remove_all()
            job = cron.new(command=self.__sampler_script)
            job.setall(self.__local_ISO_2_datetime(msmt_conf["start_time"]))
            job.enable()
            cron.write()

    def get_meassurement(self):
        # test if a meassurement is sceduled
        try:
            with open('/tmp/msmt_conf.json', 'r') as f:
                msmt_conf = json.load(f)
            return msmt_conf
        except IOError:
            return None




#con = Controller()
#con.setup_meassurement("Test2","2020-04-06T15:55:00",20,12000)
