from os import path
import json
import numpy as np
from matplotlib import pyplot as plt
from fpdf import FPDF
from datetime import datetime,timedelta

class PDF(FPDF):
    def header(self):
        # Logo
        #self.image('logo_pb.png', 10, 8, 33)
        self.set_font('Arial', '', 10)
        self.cell(0, 0, 'Base - Dosimeter Analysis Report', 0, 0, 'L')
        self.cell(0, 0, 'Measurement: ' + self.name, 0, 0, 'R')
        self.ln(20)

    # Page footer
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 10)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')

class Analyzer():

    __conf_path = "/home/pi/analyzer.json"
    __iso226_path = "/home/pi/bd_serial/ISO_226.txt"
    __win_size = None # seconds
    __fft_freq = None # np array of frequescys
    __p_ref = 20e-6 # Reference acoustic pressure [Pa]
    __N = None # Number of Samples per Window
    __fft_freq = None # np Array of frequencys
    __win = None # np Array of Window
    __sensitivity = None # Sensitivity
    __bark_freq = np.array([20,100,200,300,400,510,630,770,920,1080]) # High Cutoff bark frequency bands
    __analyzed_spectrum_db = None # np Array of analyzed db Values over full spectrum

    def __init__(self, name, signal_path):
        self.name = name
        self.signal_path = signal_path
        # load signal params
        with open(signal_path + name + '.json', 'r') as f:
            msmt_conf = json.load(f)
        self.fs = msmt_conf["fs"] # Sps
        self.duration = msmt_conf["duration"] # seconds
        self.start_time = msmt_conf["start_time"] # seconds
        if ("analyzed_spectrum_db" in msmt_conf):
            self.__load_analyzed()
            self.is_analyzed = True
        else:
            self.is_analyzed = False
        if (path.exists(signal_path + name + '.bin')):
            self.bin_exists = True
        else:
            self.bin_exists = False
        # load analyzer params
        if (path.exists(self.__conf_path)):
            with open(self.__conf_path, 'r') as f:
                ana_conf = json.load(f)
            self.__mic_calib_path = ana_conf["mic_calib_path"]
            self.__calib_path = ana_conf["calib_path"]
        else:
            # when no file is avaliable a blank one is created
            ana_conf = {}
            ana_conf["mic_calib_path"] = None
            ana_conf["calib_path"] = None
            self.__mic_calib_path = None
            self.__calib_path = None
            with open(self.__conf_path, 'w+') as f:
                json.dump(ana_conf, f)
        #plt.style.use("seaborn-whitegrid")

    def __sign_extend(self,value, bits):
        sign_bit = 1 << (bits - 1)
        return (value & (sign_bit - 1)) - (value & sign_bit)

    def get_info(self):
        info = {}
        info["name"] = self.name
        info["fs"] = self.fs
        info["start_time"] = self.start_time
        info["duration"] = self.duration
        info["duration_formated"] = self.__seconds_2_string(self.duration)
        info["is_analyzed"] = self.is_analyzed
        info["bin_exists"] = self.bin_exists
        if (self.is_analyzed):
            info["win_size"] = self.__win_size
            info["win_func"] = self.__win_func
            info["dumped_windows"] = self.__dumped_wins[0]
        return info

    # stores path for mic calibration file, not used
    def set_mic_calib_path(self, mic_calib_path):
        self.__mic_calib_path = mic_calib_path
        with open(self.__conf_path, 'r') as f:
            ana_conf = json.load(f)
        ana_conf["mic_calib_path"] = mic_calib_path
        with open(self.__conf_path, 'w+') as f:
            json.dump(ana_conf, f)

    # stores path for calibration file, not used
    def use_as_calib(self):
        self.__calib_path = self.signal_path + self.name
        with open(self.__conf_path, 'r') as f:
            ana_conf = json.load(f)
        ana_conf["calib_path"] = self.__calib_path
        with open(self.__conf_path, 'w+') as f:
            json.dump(ana_conf, f)

    def __set_window(self, win_func = "hamming", win_size_seconds = 10, win_size_sampels = None):
        if (win_size_sampels != None):
            self.__win_size = int(win_size_sampels/self.fs)
        else:
            self.__win_size = win_size_seconds
        self.__win_func = win_func
        self.__N = int(self.fs * self.__win_size) # Number of Samples per Window
        self.__fft_freq = np.fft.rfftfreq(self.__N, 1 / self.fs) # np Array of frequencys
        self.__win = getattr(np,win_func)(self.__N) # np Array of Window
        # Allowed win_funcs: hamming, hanning, blackman, bartlett

    def __get_mic_calib_dB (self):
        with open(self.__mic_calib_path,'r') as f:
            vals = f.read()
        list = vals.split()
        freq_list = []
        val_list = []
        for i in range(0,len(list),2):
            freq_list.append(float(list[i]))
            val_list.append(float(list[i+1]))
        mic_freqs = np.asarray(freq_list)
        mic_vals = np.asarray(val_list)
        fft_vals = np.interp(self.__fft_freq,mic_freqs,mic_vals) # linear interopalate values for frequencies between
        calib_val = fft_vals[self.__nearest_freq(1000)] #find Value closest to 1000 Hz
        fft_vals = fft_vals-calib_val # Normalize to 1000Hz
        return fft_vals

    def __get_sensitivity(self):

        # load signal params
        with open(self.__calib_path + '.json', 'r') as f:
            msmt_conf = json.load(f)
        fs = msmt_conf["fs"] # Sps
        duration = msmt_conf["duration"] # seconds
        # Read Signal from File
        list = []
        with open(self.__calib_path + ".bin",'rb') as f:
            i=0
            while (i<fs*duration):
                line = f.read(3)
                list.append(self.__sign_extend(line[0]<<16 | line[1] << 8 | line[2],24))
                i += 1
        signal = np.asarray(list,dtype=np.int64)
        # Calculate RMS of signal
        rms_time = np.sqrt(np.mean(np.power(signal,2)))
        sensitivity = rms_time/np.power(10,(94/20)*self.__p_ref) #this line yields nearly exactly rms_time, rounding diff because 94dB ~= 1Pa
        return sensitivity

    def __get_humm_comp(self):

        # load signal params from calibration file
        with open(self.__calib_path + '.json', 'r') as f:
            msmt_conf = json.load(f)
        fs = msmt_conf["fs"] # Sps
        duration = msmt_conf["duration"] # seconds
        n = int(duration / self.__win_size) # may not allways respect the whole calib signal but good enought
        spectrum_sum = np.zeros(self.__fft_freq.size)
        hum_comp = np.zeros(self.__fft_freq.size)
        # Read Signal from File

        with open(self.signal_path + self.name + '.bin','rb') as f:
            i=0
            while (i<n): # windows
                list=[]
                j=0
                while (j<self.__N): # Values in a window
                    line = f.read(3)
                    list.append(self.__sign_extend(line[0]<<16 | line[1] << 8 | line[2],24))
                    j += 1
                signal = np.asarray(list)
                signal = signal * self.__win # Apply window to the signal
                spectrum = np.fft.rfft(signal) # Do FFT
                spectrum *= 2 # Energy from negative frequencies missing
                spectrum = np.abs(spectrum) / np.sum(self.__win) # Scale the magnitude of FFT by window energy
                spectrum_sum += spectrum
                i += 1
        avg_spectrum = spectrum_sum/n #mean
        # Calculating hum compensation
        freq_list = [50,150]
        for freq in freq_list:
            max_val_index = avg_spectrum[self.__nearest_freq(freq-10):self.__nearest_freq(freq+10)].argmax()+self.__nearest_freq(freq-10)
            if (abs(self.__nearest_freq(freq-1) - self.__nearest_freq(freq+1))>8):
                surrounding_mean = (avg_spectrum[self.__nearest_freq(freq-1):self.__nearest_freq(freq+1)].mean())
                hum_comp[max_val_index-1] = np.abs(avg_spectrum[max_val_index-1]-surrounding_mean)
                hum_comp[max_val_index] = np.abs(avg_spectrum[max_val_index]-surrounding_mean)
                hum_comp[max_val_index+1] = np.abs(avg_spectrum[max_val_index+1]-surrounding_mean)
            else:
                # for small windows
                hum_comp[max_val_index] = np.abs(avg_spectrum[max_val_index]-((avg_spectrum[max_val_index-1]+avg_spectrum[max_val_index+1])/2))
        return hum_comp

    def __nearest_freq(self, freq):
        return np.abs(self.__fft_freq - freq).argmin()

    def __fft_spl(self):
        # ISO 45641, 4.1, (12): 10*lg((1/n)*sum(10^(0.1*Li)))
        n = int(self.duration / self.__win_size)
        #print("Windows: " + str(n))
        spectrum_sum = np.zeros(self.__fft_freq.size)
        dumped_wins = 0 # number of dumped windows

        windows_per_hour = int(3600 / self.__win_size)
        total_hours = int(self.duration / 3600)
        print(total_hours)
        print(windows_per_hour)
        div_spectrum_sum = []
        div_dumped_wins = [] #list of numbers of dumped windows for each time slot
        div_used_wins = []
        for i in range(0,total_hours):
            div_spectrum_sum.append(np.zeros(self.__fft_freq.size))
            div_dumped_wins.append(0)
            div_used_wins.append(0)
        # Read Signal from File
        with open(self.signal_path + self.name + '.bin','rb') as f:
            i=0
            acc_windows_total = 0
            acc_time_slot = 0
            while (i<n): # windows
                list=[]
                j=0
                while (j<self.__N): # Values in a window
                    line = f.read(3)
                    list.append(self.__sign_extend(line[0]<<16 | line[1] << 8 | line[2],24))
                    j += 1
                signal = np.asarray(list)
                if(signal.max() == 8388607 or signal.min() == -8388608):    # Too loud signal, window need to be dumped
                    dumped_wins += 1
                    print(acc_time_slot)
                    print(div_dumped_wins)
                    div_dumped_wins[acc_time_slot] += 1
                else:
                    signal = signal * self.__win # Apply window to the signal
                    spectrum = np.fft.rfft(signal) # Do FFT
                    spectrum *= 2 # Energy from negative frequencies missing
                    spectrum_mag = np.abs(spectrum) / np.sum(self.__win) # Scale the magnitude of FFT by window energy
                    spectrum_mag -= self.__humm_comp # Apply humm compensation
                    spectrum_mag = spectrum_mag.clip(min=1) # eliminate negative values caused by humm compensation
                    spectrum_rms = spectrum_mag / np.sqrt(2) # To obtain RMS values, divide by sqrt(2)
                    spectrum_rms[0] *= np.sqrt(2) / 2 # Do not scale the DC component

                    # Nyquist bin is not used therefore not scaled correctly
                    spectrum_db = 20 * np.log10(spectrum_rms / self.__sensitivity / self.__p_ref) # Convert to decibel scale
                    spectrum_db = spectrum_db - self.__calib_db # Compensate mic gain
                    sum_part = np.power(10,0.1*spectrum_db) # Sum part of ISO 45641 formula
                    spectrum_sum += sum_part
                    div_spectrum_sum[acc_time_slot] += sum_part

                if(acc_windows_total == windows_per_hour-1):
                    # change to new time slot
                    div_used_wins[acc_time_slot] = acc_windows_total #save number of windows used for timestamp (including dumped wins)
                    acc_windows_total = 0
                    acc_time_slot += 1
                else:
                    acc_windows_total += 1
                i += 1
        if (acc_windows_total != 0): # used when last time span was not exactly one hour long
            div_used_wins[acc_time_slot] = acc_windows_total #save number of windows used for timestamp (including dumped wins)
        avg_spectrum_db_list = [10 * np.log10(spectrum_sum/(n-dumped_wins))] # Remainings of ISO 45641 formula
        dumped_wins_list = [dumped_wins]
        for i in range(0,total_hours):
            avg_spectrum_db_list.append(10 * np.log10(div_spectrum_sum[i]/(div_used_wins[i]-div_dumped_wins[i])))
            dumped_wins_list.append(div_dumped_wins[i])

        return avg_spectrum_db_list, dumped_wins_list

    def __spl_2_phon(self,l_p): # According to ISO 226:2006-04
        alpha_f, l_u, t_f = self.__get_iso226_params()
        b_f1 = np.power((0.4 * np.power(10,((l_p+l_u)/10)-9)),alpha_f)
        b_f2 = np.power((0.4 * np.power(10,((t_f+l_u)/10)-9)),alpha_f)
        b_f = b_f1 - b_f2 + 0.005135
        l_n = 40 * np.log10(b_f) + 94
        return l_n

    def __get_iso226_params(self):
        with open(self.__iso226_path,'r') as f:
            vals = f.read()
        list = vals.split()
        freq_list = []
        alpha_f_list = []
        l_u_list = []
        t_f_list = []

        for i in range(4,len(list),4): # drop first line
            freq_list.append(float(list[i]))
            alpha_f_list.append(float(list[i+1]))
            l_u_list.append(float(list[i+2]))
            t_f_list.append(float(list[i+3]))
        # convert to numpy arrays
        iso_freqs = np.asarray(freq_list)
        iso_alpha_f = np.asarray(alpha_f_list)
        iso_l_u = np.asarray(l_u_list)
        iso_t_f = np.asarray(t_f_list)
        # linear interopalate values for frequencies between
        fft_alpha_f = np.interp(self.__fft_freq,iso_freqs,iso_alpha_f)
        fft_l_u = np.interp(self.__fft_freq,iso_freqs,iso_l_u)
        fft_t_f = np.interp(self.__fft_freq,iso_freqs,iso_t_f)
        return fft_alpha_f, fft_l_u, fft_t_f

    def __phon_2_sone(self,l_n): # according to "Psychoacoustics: Facts and Models" by Hugo Fastl and Eberhard Zwicker
        l_n = l_n.clip(min = 0) #negative phone values are set to zero
        n = np.zeros(l_n.size)
        for i in range(0,l_n.size):
            if (l_n[i] >= 40):
                n[i] = np.power(2,(0.1*l_n[i])-4)
            else:
                n[i] = np.power(l_n[i]/40,2.86) - 0.005
        return n

    def __spectrum_2_bark(self,spectrum):
        bark_avg = np.zeros(self.__bark_freq.size)
        bark_max = np.zeros(self.__bark_freq.size)
        for bark in range (1,self.__bark_freq.size): # skips first numer
            n = 0
            bark_sum = 0
            for val in range(self.__nearest_freq(self.__bark_freq[bark-1]),self.__nearest_freq(self.__bark_freq[bark])):
                bark_sum += np.power(10,0.1*spectrum[val]) # Sum part of ISO 45641 formula
                n += 1
            bark_avg[bark] = 10 * np.log10(bark_sum / n)
            bark_max[bark] = spectrum[self.__nearest_freq(self.__bark_freq[bark-1]):self.__nearest_freq(self.__bark_freq[bark])].max()
        return bark_avg, bark_max

    def analyze(self, win_func = "hamming", win_size_seconds = 10):
        if (self.is_analyzed and (win_func == self.__win_func) and (win_size_seconds == self.__win_size)):
            print(self.is_analyzed)
            print(win_size_seconds)
            print(self.__win_size)
            with open(self.signal_path + self.name + '.json', 'r') as f:
                msmt_conf = json.load(f)
            self.__analyzed_spectrum_db = np.asarray(msmt_conf["analyzed_spectrum_db"])
            print ("loaded")
        else:
            self.__set_window(win_func = win_func, win_size_seconds = win_size_seconds)
            self.__calib_db = self.__get_mic_calib_dB()
            self.__sensitivity = self.__get_sensitivity()
            self.__humm_comp = self.__get_humm_comp()
            self.__analyzed_spectrum_db, self.__dumped_wins = self.__fft_spl()
            self.is_analyzed = True
            self.__save_analyzed()
            print ("analyzed")

    def __calc_results(self):
        #create empty lists
        self.__analyzed_spectrum_phon = [None] * len(self.__analyzed_spectrum_db)
        self.__analyzed_spectrum_sone = [None] * len(self.__analyzed_spectrum_db)
        self.__analyzed_bark_avg_db = [None] * len(self.__analyzed_spectrum_db)
        self.__analyzed_bark_max_db = [None] * len(self.__analyzed_spectrum_db)
        self.__analyzed_bark_avg_phon = [None] * len(self.__analyzed_spectrum_db)
        self.__analyzed_bark_max_phon = [None] * len(self.__analyzed_spectrum_db)
        self.__analyzed_bark_avg_sone = [None] * len(self.__analyzed_spectrum_db)
        self.__analyzed_bark_max_sone = [None] * len(self.__analyzed_spectrum_db)
        for i in range(0,len(self.__analyzed_spectrum_db)):
            self.__analyzed_spectrum_phon[i] = self.__spl_2_phon(self.__analyzed_spectrum_db[i])
            self.__analyzed_spectrum_sone[i] = self.__phon_2_sone(self.__analyzed_spectrum_phon[i])
            self.__analyzed_bark_avg_db[i], self.__analyzed_bark_max_db[i] = self.__spectrum_2_bark(self.__analyzed_spectrum_db[i])
            self.__analyzed_bark_avg_phon[i], self.__analyzed_bark_max_phon[i] = self.__spectrum_2_bark(self.__analyzed_spectrum_phon[i])
            self.__analyzed_bark_avg_sone[i] = self.__phon_2_sone(self.__analyzed_bark_avg_phon[i])
            self.__analyzed_bark_max_sone[i] = self.__phon_2_sone(self.__analyzed_bark_max_phon[i])

    def __save_analyzed(self):
        with open(self.signal_path + self.name + '.json', 'r') as f:
            msmt_conf = json.load(f)
        msmt_conf["win_size"] = self.__win_size
        msmt_conf["win_func"] = self.__win_func
        msmt_conf["dumped_windows"] = self.__dumped_wins
        ana_list = []
        for i in range(0,len(self.__analyzed_spectrum_db)):
            ana_list.append(self.__analyzed_spectrum_db[i].tolist())
        msmt_conf["analyzed_spectrum_db"] = ana_list
        with open(self.signal_path + self.name + '.json', 'w+') as f:
            json.dump(msmt_conf, f)

    def __load_analyzed(self):
        with open(self.signal_path + self.name + '.json', 'r') as f:
            msmt_conf = json.load(f)
        ana_list = []
        for i in range(0,len(msmt_conf["analyzed_spectrum_db"])):
            ana_list.append(np.asarray(msmt_conf["analyzed_spectrum_db"][i]))
        self.__analyzed_spectrum_db = ana_list
        self.__dumped_wins = msmt_conf["dumped_windows"]
        self.__set_window(win_func = msmt_conf["win_func"], win_size_seconds = msmt_conf["win_size"])

    def print_spectrums(self,number,path):
        fig, (ax_spl, ax_phon, ax_sone) = plt.subplots(3, 1, figsize=(12,18))
        ax_spl.semilogx(self.__fft_freq, self.__analyzed_spectrum_db[number])
        ax_phon.semilogx(self.__fft_freq, self.__analyzed_spectrum_phon[number])
        ax_sone.semilogx(self.__fft_freq, self.__analyzed_spectrum_sone[number])
        freq_limits = (20, 1500)
        ax_spl.set_xlim( freq_limits )
        ax_phon.set_xlim( freq_limits )
        ax_sone.set_xlim( freq_limits )
        ax_spl.set_ylim( (0, 100) )
        ax_phon.set_ylim( (0, 100) )
        ax_sone.set_ylim( (0, 50) )
        ax_spl.grid('on')
        ax_phon.grid('on')
        ax_sone.grid('on')
        ax_sone.set_xlabel("Frequency [Hz]")
        ax_spl.set_ylabel("SPL [dB]")
        ax_phon.set_ylabel("Loudness Level [phon]")
        ax_sone.set_ylabel("Loudness [sone]")
        fig.savefig(path, bbox_inches='tight')
        plt.close(fig)

    def print_barks(self,number,path):
        fig, (ax_spl, ax_phon, ax_sone) = plt.subplots(3, 1, figsize=(12,18))
        x = np.arange(0,self.__bark_freq.size)
        xlabels = ["0"]
        for bark in range(1,self.__bark_freq.size):
            xlabels.append(str(bark)+" \n[" + str(self.__bark_freq[bark-1]) + "-" + str(self.__bark_freq[bark]) + "Hz]")
        width = 0.35  # the width of the bars

        spl_max = ax_spl.bar(x + width/2, self.__analyzed_bark_max_db[number],width, label="Maximum in Band")
        spl_avg = ax_spl.bar(x - width/2, self.__analyzed_bark_avg_db[number],width, label="Average of Band")
        phon_max = ax_phon.bar(x + width/2, self.__analyzed_bark_max_phon[number],width)
        phon_avg = ax_phon.bar(x - width/2, self.__analyzed_bark_avg_phon[number],width)
        sone_max = ax_sone.bar(x + width/2, self.__analyzed_bark_max_sone[number],width)
        sone_avg = ax_sone.bar(x - width/2, self.__analyzed_bark_avg_sone[number],width)
        freq_limits = (0.5, self.__bark_freq.size-0.5)
        ax_spl.set_xlim( freq_limits )
        ax_phon.set_xlim( freq_limits )
        ax_sone.set_xlim( freq_limits )
        ax_spl.set_ylim( (0, 100) )
        ax_phon.set_ylim( (0, 100) )
        ax_sone.set_ylim( (0, 50) )
        ax_sone.set_xticks(x)
        ax_sone.set_xticklabels(xlabels)
        ax_spl.grid('on')
        ax_phon.grid('on')
        ax_sone.grid('on')
        ax_sone.set_xlabel("Barkhausen Band Number")
        ax_spl.set_ylabel("SPL [dB]")
        ax_phon.set_ylabel("Loudness Level [phon]")
        ax_sone.set_ylabel("Loudness [sone]")
        self.__autolabel(ax_spl,spl_max)
        self.__autolabel(ax_spl,spl_avg)
        self.__autolabel(ax_phon,phon_max)
        self.__autolabel(ax_phon,phon_avg)
        self.__autolabel(ax_sone,sone_max)
        self.__autolabel(ax_sone,sone_avg)
        ax_spl.legend()
        fig.savefig(path, bbox_inches='tight')
        plt.close(fig)

    def print_barks_over_time(self,number,path):
        self.__calc_results()
        fig, (ax_spl, ax_phon, ax_sone) = plt.subplots(3, 1, figsize=(12,18))
        timeslots = len(self.__analyzed_bark_max_db)-1
        print(timeslots)
        x = np.arange(0,timeslots+1)
        xlabels = [self.start_time[:10] + "\n" + self.start_time[11:-3]]
        for slot in range(1,timeslots+1):
            if (slot % (int(timeslots/5)+1) == 0): #get aprox 5 numbers per diagramm
                xlabels.append((datetime.fromisoformat(self.start_time) + timedelta(hours=slot)).strftime("%H:%M"))
            else:
                xlabels.append("")
        print (x)
        print (xlabels)
        print(len(self.__get_nparray_bark__over_time(number,self.__analyzed_bark_max_db)))
        width = 1
        spl_max = ax_spl.plot(x, self.__get_nparray_bark__over_time(number,self.__analyzed_bark_max_db), drawstyle='steps-post', label="Maximum in Band " + str(number))
        spl_avg = ax_spl.plot(x, self.__get_nparray_bark__over_time(number,self.__analyzed_bark_avg_db), drawstyle='steps-post', label="Average of Band " + str(number))
        phon_max = ax_phon.plot(x, self.__get_nparray_bark__over_time(number,self.__analyzed_bark_max_phon), drawstyle='steps-post')
        phon_avg = ax_phon.plot(x, self.__get_nparray_bark__over_time(number,self.__analyzed_bark_avg_phon), drawstyle='steps-post')
        sone_max = ax_sone.plot(x, self.__get_nparray_bark__over_time(number,self.__analyzed_bark_max_sone), drawstyle='steps-post')
        sone_avg = ax_sone.plot(x, self.__get_nparray_bark__over_time(number,self.__analyzed_bark_avg_sone), drawstyle='steps-post')
        freq_limits = (0, timeslots-1)
        ax_spl.set_xlim( freq_limits )
        ax_phon.set_xlim( freq_limits )
        ax_sone.set_xlim( freq_limits )
        ax_spl.set_ylim( (0, 100) )
        ax_phon.set_ylim( (0, 100) )
        ax_sone.set_ylim( (0, 50) )
        ax_spl.set_xticks(x)
        ax_phon.set_xticks(x)
        ax_sone.set_xticks(x)
        ax_spl.set_xticklabels(xlabels)
        ax_phon.set_xticklabels(xlabels)
        ax_sone.set_xticklabels(xlabels)

        ax_spl.grid('on')
        ax_phon.grid('on')
        ax_sone.grid('on')
        ax_sone.set_xlabel("Time")
        ax_spl.set_ylabel("SPL [dB]")
        ax_phon.set_ylabel("Loudness Level [phon]")
        ax_sone.set_ylabel("Loudness [sone]")
        # self.__autolabel(ax_spl,spl_max)
        # self.__autolabel(ax_spl,spl_avg)
        # self.__autolabel(ax_phon,phon_max)
        # self.__autolabel(ax_phon,phon_avg)
        # self.__autolabel(ax_sone,sone_max)
        # self.__autolabel(ax_sone,sone_avg)
        ax_spl.legend()
        fig.savefig(path, bbox_inches='tight')
        plt.close(fig)

    def __autolabel(self,axis,bars):
        # Attach a text label above each bar in *rects*, displaying its height.
        for bar in bars:
            if (bar.get_height() < -0.05):
                height = 0
                annotext = "< 0"
            elif ((bar.get_height() < 0.05)):
                height = 0.0
                annotext = '{}'.format(round(height,1))
            else:
                height = bar.get_height()
                annotext = '{}'.format(round(height,1))

            axis.annotate(annotext,
                        xy=(bar.get_x() + bar.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')

    def __get_nparray_bark__over_time(self,bark,barks_over_time):
        list = []
        for i in range(1,len(barks_over_time)):
            list.append(barks_over_time[i][bark])
        list.append(barks_over_time[len(barks_over_time)-1][bark])
        return np.asarray(list)


    def create_pdf(self,path):
        self.analyze(win_func = self.__win_func, win_size_seconds = self.__win_size)
        self.__calc_results()
        for i in range(0,len(self.__analyzed_spectrum_db)):
            self.print_spectrums(i,"/tmp/spec" + str(i) + ".png")
            self.print_barks(i,"/tmp/bark" + str(i) + ".png")
        for bark in range(1,self.__bark_freq.size):
            self.print_barks_over_time(bark,"/tmp/bark_time" + str(bark) + ".png")
        pdf = PDF(orientation='P', unit='mm', format='A4')
        pdf.name = self.name
        pdf.alias_nb_pages()
        pdf.add_page()
        pdf.set_font("Arial", '' , size = 30)
        pdf.cell(0, 0, txt="Base - Dosimeter Analysis", ln=1, align="C")
        pdf.cell(0, 20, txt="", ln=1, align="C")
        pdf.set_font("Arial", '' , size = 20)
        pdf.cell(0, 20, txt="1. General Inforamtion", ln=1, align="L")
        pdf.set_font("Arial", '' , size = 12)
        pdf.cell(0, 5, txt="Measurement Name: " + str(self.name), ln=1, align="L")
        if (self.start_time != "now"):
            pdf.cell(0, 5, txt="Start Time:       " + datetime.fromisoformat(self.start_time).strftime("%c"), ln=1, align="L")
        pdf.cell(0, 5, txt="Duration:         " + self.__seconds_2_string(self.duration), ln=1, align="L")
        pdf.cell(0, 5, txt="Window Size:      " + str(self.__win_size) + " seconds", ln=1, align="L")
        pdf.cell(0, 5, txt="Window Function:  " + str(self.__win_func).capitalize() + " Window", ln=1, align="L")
        pdf.add_page()
        pdf.set_font("Arial", '' , size = 20)
        pdf.cell(0, 0, txt="2. Analysis: Spectrums", ln=1, align="L")
        pdf.image("/tmp/spec0.png", x = 15, y = 40, h = 240)
        pdf.add_page()
        pdf.set_font("Arial", '' , size = 20)
        pdf.cell(0, 0, txt="3. Analysis: Barkhausen Bands", ln=1, align="L")
        pdf.image("/tmp/bark0.png", x = 15, y = 40, h = 240)
        if (self.start_time != "now"):
            pdf.add_page()
            pdf.set_font("Arial", '' , size = 20)
            pdf.cell(0, 0, txt="4. Analysis: Time Slots", ln=1, align="L")
            pdf.cell(0, 20, txt="", ln=1, align="C")
            for i in range(1,len(self.__analyzed_spectrum_db)):
                start_str = (datetime.fromisoformat(self.start_time) + timedelta(hours=i-1)).strftime("%x %X")
                end_str = (datetime.fromisoformat(self.start_time) + timedelta(hours=i)).strftime("%x %X")
                pdf.add_page()
                pdf.set_font("Arial", '' , size = 16)
                pdf.cell(0, 0, txt="4." + str(i) + ". Spectrums from " + start_str + " to " + end_str, ln=1, align="L")
                pdf.image("/tmp/spec" + str(i) + ".png", x = 15, y = 40, h = 240)
                pdf.add_page()
                pdf.cell(0, 0, txt="4." + str(i) + ". B. Bands from " + start_str + " to " + end_str, ln=1, align="L")
                pdf.image("/tmp/bark" + str(i) + ".png", x = 15, y = 40, h = 240)
            pdf.add_page()
            pdf.set_font("Arial", '' , size = 20)
            pdf.cell(0, 0, txt="5. Analysis: Barkhausen Bands over Time", ln=1, align="L")
            pdf.cell(0, 20, txt="", ln=1, align="C")
            for bark in range(1,self.__bark_freq.size):
                band_text = str(bark) + " [" + str(self.__bark_freq[bark-1]) + "-" + str(self.__bark_freq[bark]) + "Hz]"
                pdf.add_page()
                pdf.set_font("Arial", '' , size = 16)
                pdf.cell(0, 0, txt="5." + str(bark) + ". Barkhausen Band " + band_text, ln=1, align="L")
                pdf.image("/tmp/bark_time" + str(bark) + ".png", x = 15, y = 40, h = 240)

        pdf.output(path)

    def __seconds_2_string(self,sec):
        days, rem = divmod(sec, 3600*24)
        hours, rem = divmod(rem, 3600)
        min, sec = divmod(rem, 60)
        s =  str(int(days)) + " days, " + str(int(hours)) + " hours, " + str(int(min)) + " minutes, "
        return s

#ana = Analyzer("test4h","/home/pi/msmts/")
# ana.use_as_calib()
# ana.set_mic_calib_path("/home/pi/35B407_cal_0degree.txt")
#ana.analyze(win_func="hamming",win_size_seconds = 20)
#ana.print_spectrums("../spectrums.png")
#ana.print_barks_over_time(2,"../barks_time.png")
#ana.create_pdf("/home/pi/testpdf.pdf")
