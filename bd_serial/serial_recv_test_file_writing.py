import max11040k
import time
import numpy as np
from matplotlib import pyplot as plt

adc = max11040k.Max11040k()
adc.setup(2000000,'/dev/ttyAMA0')
adc.pauseSampling()
adc.config([adc.EN24BIT,adc.XTALEN],12000)
adc.beginSampling()
with open('workfile','wb') as f:
    i=0
    while (i<=12000):
        f.write(adc.getSampleBytes())
        i += 1
adc.pauseSampling()
list=[]
with open('workfile','rb') as f:
    i=0
    while (i<=12000):
        line = f.read(3)
        if(i>=1000 and i<1024):
            list.append(max11040k.sign_extend(line[0]<<16 | line[1] << 8 | line[2],24))
        i += 1
x = np.arange(0,24/12000,1/12000)
y = np.asarray(list)
plt.title("Matplotlib demo")
plt.xlabel("x axis caption")
plt.ylabel("y axis caption")
plt.plot(x,y,'ob')
plt.savefig("myfig.png", bbox_inches='tight')
