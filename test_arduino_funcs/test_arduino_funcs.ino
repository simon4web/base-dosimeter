uint16_t sensorValue;
uint16_t start_time,end_time;

void setup() {
  Serial.begin(115200);
  
}

void loop() {
  start_time = micros();
  sensorValue = analogRead(A0);
  Serial.write((sensorValue>>8));
  Serial.write(sensorValue);
  sensorValue = analogRead(A1);
  Serial.write((sensorValue>>8));
  Serial.write(sensorValue);
  sensorValue = analogRead(A2);
  Serial.write((sensorValue>>8));
  Serial.write(sensorValue);
  sensorValue = analogRead(A3);
  Serial.write((sensorValue>>8));
  Serial.write(sensorValue);
  end_time = micros();
  Serial.println("");
  Serial.println(start_time-end_time);
}
